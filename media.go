package m3u8

import (
	//"errors"
	//"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"io"
)

/*
Not a proper implementation of an m3u parser. We only care about the files
listed as well as any EXT tags that may belong to them.

http://tools.ietf.org/html/draft-pantos-http-live-streaming-08

In order to read both a stream and a vod we need to store
EXT-X-MEDIA-SEQUENCE:<index of first media segment in the playlist>.

*/

// MediaPlaylist contains the list of files along with their EXT tags.
type MediaPlaylist struct {
	Sequence int
	Segments []MediaSegment
}

// MediaSegment wraps a URI and its duration
type MediaSegment struct {
	URI      string
	Duration time.Duration
}

// ParseMediaPlaylist data from reader into a list of files we can load or stream.
func ParseMediaPlaylist(r io.Reader) (*MediaPlaylist, error) {
	ld, err := decode(r)
	if err != nil {
		return nil, err
	}

	playlist := &MediaPlaylist{}

	// Build up MediaSegment, ending when URI is found.
	seg := MediaSegment{Duration: -1}

	for _, l := range ld.GetLines() {

		switch l.Type {
		case Tag:
			t := l.Data.(TagLine)
			if t.Tag == "EXTINF" { // Segment duration

				spl := strings.Split(string(t.Args), ",")
				dur, err := strconv.ParseFloat(spl[0], 64)
				if err != nil {
					log.Printf("Unable to parse extinfo duration: %s %q", string(t.Args), err)
					continue
				}

				seg.Duration = time.Second * time.Duration(dur)

			} else if t.Tag == "EXT-X-MEDIA-SEQUENCE" {
				seq, err := strconv.Atoi(string(t.Args))
				if err != nil {
					log.Printf("Unable to parse media sequence %s", string(t.Args))
					return nil, err
				}
				playlist.Sequence = seq
			} else if t.Tag == "EXT-X-ENDLIST" { // End of media, not required
				return playlist, nil
			}
			break

		case URI:
			if seg.Duration == -1 {
				continue
			}

			seg.URI = l.Data.(string)
			playlist.Segments = append(playlist.Segments, seg)

			seg = MediaSegment{Duration: -1}
			break
		}

	}

	if len(playlist.Segments) == 0 {
		return nil, fmt.Errorf("Invalid M3U8 - no URIs found")
	}

	return playlist, nil

}
