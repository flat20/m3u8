# README #

M3U8 can parse HTTP live streaming m3u8 playlists, and stream their media contents. It only reads the minimum info required to download the media specified in the M3U8 file. As such, it doesn't check the validity of the M3U8 file and ignores irrelevant EXT tags.

### Get setup ###

go get -u bitbucket.org/flat20/m3u8

examples/ contain two apps which demonstrate two use cases for the m3u8 library.

go install bitbucket.org/flat20/m3u8/examples/svtplay
go install bitbucket.org/flat20/m3u8/examples/twitch

### Usage ###

To download the contents of a media playlist .m3u8 file first parse it into a Media Playlist. Then create a m3u8.MPReader io.Reader and pass it a http.Client. The reader can then be used in the usual way to download the media.

https://bitbucket.org/flat20/m3u8/src/23cff8ce3b5eb25f7ccd4ca32221cc2f5bd25632/streamer_test.go?at=master#cl-14

~~~~
	var s = `#EXTM3U
#EXT-X-TARGETDURATION:10
#EXT-X-MEDIA-SEQUENCE:1
#EXTINF:10.000,
http://www.mydomain.com/seg1.mp4
#EXTINF:10.000,
http://www.mydomain.com/seg2.mp4
#EXTINF:10.000,
http://www.mydomain.com/seg3.mp4`

	r := strings.NewReader(s)
	pl, err := ParseMediaPlaylist(r)

	mr := NewMPReader(pl, client, pl.Sequence)

	written, err := io.Copy(ioutil.Discard, mr)
	if err != nil {
		t.Fatalf("Error copying: %q", err)
	}
~~~~

### Further info ###

HTTP Live Streaming specifications: https://tools.ietf.org/html/draft-pantos-http-live-streaming-16

Contact andreas.reuterberg@gmail.com with any questions.