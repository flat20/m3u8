package m3u8

import (
	//"errors"
	//"errors"
	"fmt"

	"io"
)

// MasterPlaylist describes a set of Media variants
type MasterPlaylist struct {
	Groups []MediaGroup
}

// MediaGroup contains our media info
type MediaGroup struct {
	Name       string
	StreamInfo map[string]string
	URI        string
}

// ParseMasterPlaylist returns a completed MasterPlaylist decoded from r
func ParseMasterPlaylist(r io.Reader) (*MasterPlaylist, error) {
	ld, err := decode(r)
	if err != nil {
		return nil, err
	}

	playlist := &MasterPlaylist{}

	// Build up the MediaSegment, ending when URI is found.
	group := MediaGroup{}

	for _, l := range ld.GetLines() {

		switch l.Type {
		case Tag:
			t := l.Data.(TagLine)

			if t.Tag == "EXT-X-MEDIA" {

				attrs, err := t.Args.AttrList()
				if err != nil {
					return nil, fmt.Errorf("Unable to parse attr list %q %q", t.Args, err)
				}

				if name, ok := attrs["NAME"]; ok {
					group.Name = name
				} else {
					return nil, fmt.Errorf("Unable to find group name attribute. %q", t.Args)
				}

			} else if t.Tag == "EXT-X-STREAM-INF" {
				attrs, err := t.Args.AttrList()
				if err != nil {
					return nil, fmt.Errorf("Unable to parse attr list %q %q", t.Args, err)
				}
				group.StreamInfo = attrs

			}
			break

		case URI:
			/*
				if group.Name == "" {
					continue
				}
			*/
			group.URI = l.Data.(string)

			playlist.Groups = append(playlist.Groups, group)
			// Remember group name
			group = MediaGroup{Name: group.Name}
			break
		}
	}

	if len(playlist.Groups) == 0 {
		return nil, fmt.Errorf("Invalid M3U8 master playlist - no URIs found")
	}

	return playlist, nil

}
