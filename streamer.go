package m3u8

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"
)

// Possibly replace *http.Client with an interface or a func like this:
// type URLLoader func(URL string) (io.ReadCloser, error)

// TODO Need to support a base URL for playlists with relative URLs.

// MediaReader MediaPlaylist io.Reader
type MediaReader struct {
	list   *MediaPlaylist
	client *http.Client // TODO Replace with URILoader or similar interface
	//read     SegReader
	buffer   []byte // Internal storage
	start    int    //starting from sequence
	sequence int    // current sequence
	end      int    // Last sequence
}

// NewMediaReader creates a MediaReader for the supplied list, starting reading
// at the given sequence.
func NewMediaReader(list *MediaPlaylist, client *http.Client, startSeq int) *MediaReader {
	return &MediaReader{list: list, client: client, buffer: make([]byte, 0, 1024*100),
		start: startSeq, sequence: startSeq,
		end: list.Sequence + len(list.Segments)}
}

// Read the next segment
func (mr *MediaReader) Read(out []byte) (n int, err error) {

	// No space for anything
	if len(out) == 0 {
		return
		//return 0, nil
	}

	// Load more if we have no data left in our buffer
	if len(mr.buffer) == 0 {

		// cache?
		if mr.sequence >= mr.end {
			return 0, io.EOF
		}

		// TODO Composite this, maybe getSeg could be passed in?
		mr.buffer, err = getSeg(mr, mr.client, mr.sequence)
		if err != nil {
			return
		}

		mr.sequence++
	}

	n = copy(out, mr.buffer)
	mr.buffer = mr.buffer[n:]

	return

}

// GET a segment into a []byte slice, sequence is pl.Sequence + index
func getSeg(mr *MediaReader, client *http.Client, sequence int) ([]byte, error) {

	i := sequence - mr.list.Sequence
	seg := mr.list.Segments[i]

	reader, err := loadURL(client, seg.URI)
	if err != nil {
		return nil, fmt.Errorf("Unable to load media segment from %s: %q", seg.URI, err)
	}

	b, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err //fmt.Errorf("Unable to read media segment data %q", err)
	}
	reader.Close()

	return b, nil

}

// Streamer Implements the Reader interface and reloads a master m3u8 playlist
// continuing download where the previous playlist ended
type Streamer struct {
	client *http.Client
	source *url.URL

	reloadRate time.Duration // Only reload media playlist every so often
	lastLoad   time.Time
	mReader    *MediaReader
	nextSeq    int // Sequence to start at for next playlist

}

// NewStreamer Creates a Streamer which downloads a Twitch stream from
// the source URL
func NewStreamer(client *http.Client, playlistURL string) *Streamer {

	source, err := url.Parse(playlistURL)
	if err != nil {
		log.Fatal(fmt.Sprintf("Unable to parse url %s %q", playlistURL, err))
	}

	return &Streamer{mReader: nil,
		source: source, client: client,
		reloadRate: time.Second * 2,
	}
}

// Read from source until an error is encountered.
func (s *Streamer) Read(p []byte) (int, error) {

	var err error

	// Update our media playlist reader if needed
	if s.mReader == nil {

		// Reload playlist if enough time has past
		if time.Now().Sub(s.lastLoad) < s.reloadRate {
			return 0, nil
		}

		s.mReader, err = s.updateReader(s.nextSeq)
		if err != nil {
			return 0, err
		}

		s.lastLoad = time.Now()
	}

	// Read from media playlist
	n, err := s.mReader.Read(p)
	if err != nil {
		if err == io.EOF {
			//log.Printf("Completed one media playlist. %d < %d < %d", s.mReader.start, s.mReader.sequence, s.mReader.end)
			s.nextSeq = s.mReader.sequence
			s.mReader = nil
		} else {
			return 0, err
		}

	}
	return n, nil

}

// Reloads the internal media playlist and fixes relative URIs.
func (s *Streamer) updateReader(startSeq int) (*MediaReader, error) {

	list, err := readMediaPlaylist(s.client, s.source)
	if err != nil {
		return nil, fmt.Errorf("Unable to load media playlist: %q", err)
	}

	// Clone source URL and store as a base URL
	// so we can use it for loading relative URIS
	bu, err := url.Parse(s.source.String())
	if err != nil {
		return nil, fmt.Errorf("Unable to parse url %s %q", s.source.String(), err)
	}
	bu.Path = path.Dir(bu.Path)
	bu.RawQuery = ""
	bu.Fragment = ""
	baseURL := bu

	// Absolute relative URIs.
	for i, seg := range list.Segments {

		var completeURL string
		if strings.HasPrefix(seg.URI, "http") {
			completeURL = seg.URI
		} else {
			completeURL = fmt.Sprintf("%s/%s", baseURL.String(), seg.URI)
		}
		list.Segments[i].URI = completeURL
	}

	if startSeq < list.Sequence {
		startSeq = list.Sequence
	}

	mReader := NewMediaReader(list, s.client, startSeq)

	//log.Printf("New list reader %d < %d < %d", mReader.start, mReader.sequence, mReader.end)

	return mReader, nil
}

// readMediaPlaylist Get Stream media segments playlist.
func readMediaPlaylist(client *http.Client, URL *url.URL) (*MediaPlaylist, error) {

	bodyReader, err := loadURL(client, URL.String())
	if err != nil {
		return nil, fmt.Errorf("load playlist from %s error: %q", URL.String(), err)
	}
	defer bodyReader.Close()

	pl, err := ParseMediaPlaylist(bodyReader)
	if err != nil {
		return nil, fmt.Errorf("Unable to parse playlist from %s: %q", URL, err)
	}

	return pl, nil

}

// Helper for http GET using a http.Client, or something implementing
// the HTTPLoader interface
func loadURL(client *http.Client, URL string) (io.ReadCloser, error) {

	resp, err := client.Get(URL)
	if err != nil {
		return nil, fmt.Errorf("Couldn't GET %s. %v", URL, err)
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("Status code %d", resp.StatusCode)
	}

	return resp.Body, nil
}

// Helper function to create absolute URLs from relative URIs
// src is the source URL the uri lives in if it's a relative.
func makeAbs(src string, uri string) (string, error) {

	t, err := url.Parse(uri)
	if err != nil {
		return "", fmt.Errorf("Unable to parse URI %s %q", uri, err)
	}

	// Already is absolute
	if t.IsAbs() {
		return uri, nil
	}

	// Clean src up to its base path.
	bu, err := url.Parse(src)
	if err != nil {
		return "", fmt.Errorf("Unable to parse url %s %q", src, err)
	}
	bu.RawQuery = ""
	bu.Fragment = ""

	// Append URI
	bu.Path = fmt.Sprintf("%s/%s", path.Dir(bu.Path), uri)

	return bu.String(), nil

}
