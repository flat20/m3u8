package main

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"time"

	"bitbucket.org/flat20/m3u8"
)

// AccessTokenResponse API token request response
type AccessTokenResponse struct {
	Token            string `json:"token"`
	Signature        string `json:"sig"`
	MobileRestricted bool   `json:"mobile_restricted"`
}

// StreamChannelResponse API stream channel request response
type StreamChannelResponse struct {
	Stream *Stream `json:"stream"`
}

// Stream data returned in StreamChannelResponse
type Stream struct {
	Game       string  `json:"game"`
	Viewers    int     `json:"viewers"`
	AverageFPS float64 `json:"average_fps"`
	ViewHeight int     `json:"video_height"`
	CreatedAt  string  `json:"created_at"`
}

// GetAccessToken returns Twitch API token response
func GetAccessToken(client *http.Client, channel string) (*AccessTokenResponse, error) {

	var res AccessTokenResponse

	URL := fmt.Sprintf("http://api.twitch.tv/api/channels/%s/access_token", channel)
	err := loadAndDecode(client, URL, &res)
	if err != nil {
		return nil, fmt.Errorf("Couldn't get access token: %q", err)
	}

	return &res, nil
}

// GetStreamChannel returns stream data, such as view count
func GetStreamChannel(client *http.Client, channel string) (*StreamChannelResponse, error) {

	var res StreamChannelResponse

	URL := fmt.Sprintf("https://api.twitch.tv/kraken/streams/%s", channel)
	err := loadAndDecode(client, URL, &res)
	if err != nil {
		return nil, fmt.Errorf("Couldn't get stream channel: %q", err)
	}

	if res.Stream == nil {
		return nil, fmt.Errorf("Stream not online")
	}

	return &res, nil
}

// GetStreamQualityM3U Fetch the M3U containing the stream list.
// http://usher.twitch.tv/api/channel/hls/{channel}.m3u8?player=twitchweb&&token={token}&sig={sig}&allow_audio_only=true&allow_source=true&type=any&p={random}
func GetStreamQualityM3U(client *http.Client, channel string, token *AccessTokenResponse) (*m3u8.MasterPlaylist, error) {

	rng := rand.New(rand.NewSource(time.Now().UnixNano()))
	r := rng.Intn(90000)

	URL := fmt.Sprintf("http://usher.twitch.tv/api/channel/hls/%s.m3u8?player=twitchweb&&token=%s&sig=%s&allow_audio_only=true&allow_source=true&type=any&p=%d", channel, token.Token, token.Signature, r)
	body, err := loadURL(client, URL)
	if err != nil {
		return nil, err
	}

	pl, err := m3u8.ParseMasterPlaylist(body)
	if err != nil {
		return nil, fmt.Errorf("Unable to parse playlist from %s: %q", URL, err)
	}

	return pl, nil

}

// Helper to load and decode JSON from a URL
func loadAndDecode(client *http.Client, URL string, res interface{}) error {

	// Get HTTP Body reader
	bodyReader, err := loadURL(client, URL)
	if err != nil {
		return fmt.Errorf("loadURL error: %q", err)
	}
	defer bodyReader.Close()

	// Decode from JSON
	dec := json.NewDecoder(bodyReader)
	err = dec.Decode(res)
	if err != nil {
		return fmt.Errorf("Not StreamChannelResponse: %q", err)
	}

	return nil
}

// Helper for http GET using a http.Client, or something implementing
// the HTTPLoader interface
func loadURL(client *http.Client, URL string) (io.ReadCloser, error) {

	resp, err := client.Get(URL)
	if err != nil {
		return nil, fmt.Errorf("Couldn't GET %s. %v", URL, err)
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("Status code %d", resp.StatusCode)
	}

	return resp.Body, nil
}
