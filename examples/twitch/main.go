package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	_ "expvar"

	"bitbucket.org/flat20/m3u8"
)

func main() {
	var channel string
	var output string
	var quality string
	flag.StringVar(&channel, "channel", "", "Twitch channel name")
	flag.StringVar(&quality, "quality", "Source", "Stream quality")
	flag.StringVar(&output, "output", "out.ts", "Output file name")
	flag.Parse()

	if channel == "" {
		flag.PrintDefaults()
		return
	}
	channel = strings.ToLower(channel)

	log.Printf("Saving %s in %s quality to %s", channel, quality, output)

	client := &http.Client{Timeout: time.Second * 10}

	info, err := GetStreamChannel(client, channel)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Streaming %s to %d viewers", info.Stream.Game, info.Stream.Viewers)

	playlist, err := getPlaylist(client, channel, quality)
	if err != nil {
		log.Fatal(err)
	}

	go http.ListenAndServe(":1234", nil)

	stream(client, output, playlist)

}

func getPlaylist(client *http.Client, channel string, quality string) (string, error) {
	token, err := GetAccessToken(client, channel)
	if err != nil {
		return "", fmt.Errorf("access token error: %q", err)
	}

	qual, err := GetStreamQualityM3U(client, channel, token)
	if err != nil {
		return "", fmt.Errorf("master playlist error: %q", err)
	}

	quality = strings.ToLower(quality)
	for _, v := range qual.Groups {
		log.Printf("Quality %s", v.Name)
		if strings.ToLower(v.Name) == quality {
			log.Printf("Using %s quality", v.Name)
			return v.URI, nil
		}
	}

	return "", fmt.Errorf("Unable to find quality %s", quality)

}

func stream(client *http.Client, output string, playlist string) {
	out, err := os.OpenFile(output, os.O_CREATE|os.O_WRONLY, 0755)
	if err != nil {
		log.Fatal(fmt.Sprintf("unable to open output file %q", err))
	}
	defer out.Close()

	s := m3u8.NewStreamer(client, playlist)

	//_, err = io.Copy(out, s)
	_, err = copyProg(out, s)
	if err != nil {
		log.Printf("Error reading all from stream: %q", err)
		return
	}

}

// io.Copy but print progress
func copyProg(dst io.Writer, src io.Reader) (written int64, err error) {
	buf := make([]byte, 32*1024)
	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
				fmt.Printf("\rWrote %.2f MB", float64(written)/1024/1024)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er == io.EOF {
			break
		}
		if er != nil {
			err = er
			break
		}
	}
	return written, err
}
