package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/flat20/m3u8"
)

type info struct {
	VideoID int     `json:"videoId"`
	Video   video   `json:"video"`
	Context context `json:"context"`
}

type video struct {
	References []references `json:"videoReferences"`
}

type references struct {
	URL        string `json:"url"`
	PlayerType string `json:"playerType"`
}

type context struct {
	Title string `json:"title"`
}

// Select a media playlist from the master list using the desired resolution
func selectPlaylist(client *http.Client, masterURL string, resolution string) (string, error) {

	r, err := loadURL(client, masterURL)
	if err != nil {
		return "", err
	}
	defer r.Close()

	// Sadly svtplay's m3u's have a CR and not \r\n after the #EXTM3U tag
	d, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}

	fixed := strings.Replace(string(d), "\r", "\r\n", 1)
	fr := strings.NewReader(fixed)

	pl, err := m3u8.ParseMasterPlaylist(fr)
	if err != nil {
		return "", fmt.Errorf("Unable to parse playlist from %s: %q", masterURL, err)
	}

	// Try to find the desired resolution at the highest bandwidth
	var g *m3u8.MediaGroup
	bestBw := 0
	for _, v := range pl.Groups {
		b, _ := strconv.Atoi(v.StreamInfo["BANDWIDTH"])
		bw := int(math.Floor(float64(b) / 1000))
		res := v.StreamInfo["RESOLUTION"]
		log.Printf("%s %dk", res, bw)
		if res == resolution && bw > bestBw {
			g = &v
			bestBw = bw
		}
	}

	// Fallback on first entry

	if g == nil {
		return "", fmt.Errorf("Couldn't find desired resolution")
	}

	log.Printf("Picked %s %dk %s", g.StreamInfo["RESOLUTION"], bestBw, g.URI)
	return g.URI, nil

}

// Try to find a suitable playlist we can download
func getInfo(client *http.Client, ID int) (*info, error) {
	var vi info
	iu := fmt.Sprintf("http://www.svtplay.se/video/%d?output=json", ID)
	err := decode(client, iu, &vi)
	if err != nil {
		return nil, err
	}

	return &vi, nil

}

// Helper to load and decode JSON from a URL
func decode(client *http.Client, URL string, res interface{}) error {

	// Get HTTP Body reader
	bodyReader, err := loadURL(client, URL)
	if err != nil {
		return fmt.Errorf("loadURL error: %q", err)
	}
	defer bodyReader.Close()

	// Decode from JSON
	dec := json.NewDecoder(bodyReader)
	err = dec.Decode(res)
	if err != nil {
		return fmt.Errorf("Not StreamChannelResponse: %q", err)
	}

	return nil
}

// Helper for http GET using a http.Client, or something implementing
// the HTTPLoader interface
func loadURL(client *http.Client, URL string) (io.ReadCloser, error) {

	resp, err := client.Get(URL)
	if err != nil {
		return nil, fmt.Errorf("Couldn't GET %s. %v", URL, err)
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("Status code %d", resp.StatusCode)
	}

	return resp.Body, nil
}
