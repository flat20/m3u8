package main

/*


http://www.svtplay.se/video/1199346?output=json

http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/master.m3u8?cc1=name=Svenska~default=yes~forced=no~uri=http://media.svt.se/download/mcc/wp3/undertexter-wsrt/1207696/1207696-001A/C(sv)/index.m3u8~lang=sv
*/

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"time"

	"bitbucket.org/flat20/m3u8"
)

func main() {
	var videoID int
	var resolution string
	var output string
	flag.IntVar(&videoID, "video", 0, "Video ID. For example 1199346")
	flag.StringVar(&resolution, "resolution", "1280x720", "Video resolution")
	flag.StringVar(&output, "output", "out.ts", "Output file name")
	flag.Parse()

	if videoID == 0 {
		flag.PrintDefaults()
		return
	}

	log.Printf("Saving %d to %s", videoID, output)

	client := &http.Client{Timeout: time.Second * 10}

	url, err := getMasterURL(client, videoID)
	if err != nil {
		log.Fatal(err)
	}

	playlist, err := getPlaylist(client, url, resolution)
	if err != nil {
		log.Fatal(err)
	}

	download(client, output, playlist)

}

// Try to find a suitable playlist we can download
func getMasterURL(client *http.Client, ID int) (string, error) {
	vi, err := getInfo(client, ID)
	if err != nil {
		return "", err
	}

	log.Printf("%s", vi.Context.Title)

	// Hoping a URL contains .m3u8
	for _, r := range vi.Video.References {
		if strings.Contains(r.URL, ".m3u8") {
			return r.URL, nil
		}
	}

	return "", nil

}

// Get the media playlist from the master URL
func getPlaylist(client *http.Client, master string, resolution string) (*m3u8.MediaPlaylist, error) {

	u, err := url.Parse(master)
	if err != nil {
		return nil, err
	}

	//bi := strings.LastIndex(u.Path, "/")
	//basePath := u.Path[:bi+1]
	baseURL := fmt.Sprintf("%s://%s%s/", u.Scheme, u.Host, path.Dir(u.Path))

	url, err := selectPlaylist(client, master, resolution)
	if err != nil {
		return nil, err
	}

	if !strings.HasPrefix(url, "http") {
		url = fmt.Sprintf("%s%s", baseURL, url)
	}

	r, err := loadURL(client, url)
	if err != nil {
		return nil, err
	}
	pl, err := m3u8.ParseMediaPlaylist(r)
	if err != nil {
		return nil, err
	}

	// This is awful.
	for i, v := range pl.Segments {
		if !strings.HasPrefix(v.URI, "http") {
			pl.Segments[i].URI = fmt.Sprintf("%s%s", baseURL, v.URI)
		}
	}

	return pl, nil
}

// download all media from the playlist
func download(client *http.Client, output string, pl *m3u8.MediaPlaylist) {

	out, err := os.OpenFile(output, os.O_CREATE|os.O_WRONLY, 0755)
	if err != nil {
		log.Fatal(fmt.Sprintf("unable to open output file %q", err))
	}
	defer out.Close()

	mr := m3u8.NewMediaReader(pl, client, pl.Sequence)

	//_, err = io.Copy(out, mr)
	_, err = copyProg(out, mr)
	if err != nil {
		log.Printf("Error reading all from stream: %q", err)
		return
	}

}

// io.Copy but print progress
func copyProg(dst io.Writer, src io.Reader) (written int64, err error) {
	buf := make([]byte, 32*1024)
	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
				fmt.Printf("\rWrote %.2f MB", float64(written)/1024/1024)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er == io.EOF {
			break
		}
		if er != nil {
			err = er
			break
		}
	}
	return written, err
}
