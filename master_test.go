package m3u8

import (
	"strings"
	"testing"
)

func TestMasterPlaylist(t *testing.T) {
	var s = `#EXTM3U
#EXT-X-TWITCH-INFO:NODE="video26.lhr02",MANIFEST-NODE="video26.lhr02",SERVER-TIME="1435228086.79",USER-IP="195.171.175.178",CLUSTER="lhr02",MANIFEST-CLUSTER="lhr02"
#EXT-X-MEDIA:TYPE=VIDEO,GROUP-ID="chunked",NAME="Source",AUTOSELECT=YES,DEFAULT=YES
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=3347835,RESOLUTION=1920x1080,VIDEO="chunked"
http://video26.lhr02.hls.ttvnw.net/hls-833f38/twoeasy_15016419568_262036819/chunked/py-index-live.m3u8?token=id=873875433480195067,bid=15016419568,exp=1435314486,node=video26-1.lhr02.hls.justin.tv,nname=video26.lhr02,fmt=chunked&sig=f2315ae537b57dc9e61d297cf89ac3a59c198c1b
#EXT-X-MEDIA:TYPE=VIDEO,GROUP-ID="high",NAME="High",AUTOSELECT=YES,DEFAULT=YES
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=1760000,VIDEO="high"
http://video26.lhr02.hls.ttvnw.net/hls-833f38/twoeasy_15016419568_262036819/high/py-index-live.m3u8?token=id=873875433480195067,bid=15016419568,exp=1435314486,node=video26-1.lhr02.hls.justin.tv,nname=video26.lhr02,fmt=high&sig=e79b243755ae391c2c186963c982e2bc3fd39928
#EXT-X-MEDIA:TYPE=VIDEO,GROUP-ID="medium",NAME="Medium",AUTOSELECT=YES,DEFAULT=YES
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=928000,VIDEO="medium"
http://video26.lhr02.hls.ttvnw.net/hls-833f38/twoeasy_15016419568_262036819/medium/py-index-live.m3u8?token=id=873875433480195067,bid=15016419568,exp=1435314486,node=video26-1.lhr02.hls.justin.tv,nname=video26.lhr02,fmt=medium&sig=cda3e2fa8ffff555da5ece19f013876bd6fb15fb
#EXT-X-MEDIA:TYPE=VIDEO,GROUP-ID="low",NAME="Low",AUTOSELECT=YES,DEFAULT=YES
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=596000,VIDEO="low"
http://video26.lhr02.hls.ttvnw.net/hls-833f38/twoeasy_15016419568_262036819/low/py-index-live.m3u8?token=id=873875433480195067,bid=15016419568,exp=1435314486,node=video26-1.lhr02.hls.justin.tv,nname=video26.lhr02,fmt=low&sig=aa7a1ddc7cd1c4f50e9898a8cfa376886d0b57e5
#EXT-X-MEDIA:TYPE=VIDEO,GROUP-ID="mobile",NAME="Mobile",AUTOSELECT=YES,DEFAULT=YES
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=164000,VIDEO="mobile"
http://video26.lhr02.hls.ttvnw.net/hls-833f38/twoeasy_15016419568_262036819/mobile/py-index-live.m3u8?token=id=873875433480195067,bid=15016419568,exp=1435314486,node=video26-1.lhr02.hls.justin.tv,nname=video26.lhr02,fmt=mobile&sig=4dce1f6e718879411cdc7f61ec7e0b87da6fbe77`

	r := strings.NewReader(s)
	m3u, err := ParseMasterPlaylist(r)
	if err != nil {
		t.Fatalf("Unable to parse m3u %q", err)
	}

	if len(m3u.Groups) != 5 {
		t.Fatalf("Didn't parse exactly 5 File: %d", len(m3u.Groups))
	}

	if m3u.Groups[0].URI != `http://video26.lhr02.hls.ttvnw.net/hls-833f38/twoeasy_15016419568_262036819/chunked/py-index-live.m3u8?token=id=873875433480195067,bid=15016419568,exp=1435314486,node=video26-1.lhr02.hls.justin.tv,nname=video26.lhr02,fmt=chunked&sig=f2315ae537b57dc9e61d297cf89ac3a59c198c1b` {
		t.Fatalf("Unable to parse filename: %s", m3u.Groups[0].URI)
	}

	si := m3u.Groups[0].StreamInfo

	video, exists := si["VIDEO"]
	if !exists {
		t.Fatalf("Unable to parse VIDEO attribute: %+v", m3u.Groups[0])
	}

	if video != "chunked" {
		t.Fatalf("Incorrect video %q", video)
	}

	if si["BANDWIDTH"] != "3347835" {
		t.Fatalf("Unable to parse BANDWIDTH: %+v", si["BANDWIDTH"])
	}

}

func TestParseBad(t *testing.T) {
	parseBad(t, `<ul>
asdasd nodata
</ul>`)

	parseBad(t, `#APA-HEJ:676=sdasd,3536=sd`)

}

func parseBad(t *testing.T, s string) {
	r := strings.NewReader(s)
	m, err := ParseMediaPlaylist(r)
	if err == nil {
		t.Fatalf("M3U error not detected in %s.\n%+v", s, m)
	}
}

func TestSVT(t *testing.T) {
	s := `#EXTM3U
#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID="subs",NAME="Svenska",DEFAULT=YES,FORCED=NO,URI="http://media.svt.se/download/mcc/wp3/undertexter-wsrt/1207696/1207696-001A/C(sv)/index.m3u8",LANGUAGE="sv"
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=985000,RESOLUTION=1280x720,CODECS="avc1.77.30, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_0_av.m3u8?null=
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=237000,RESOLUTION=512x288,CODECS="avc1.66.30, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_1_av.m3u8?null=
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=345000,RESOLUTION=512x288,CODECS="avc1.66.30, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_2_av.m3u8?null=
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=453000,RESOLUTION=512x288,CODECS="avc1.66.30, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_3_av.m3u8?null=
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=633000,RESOLUTION=512x288,CODECS="avc1.66.30, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_4_av.m3u8?null=
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=1677000,RESOLUTION=1280x720,CODECS="avc1.77.30, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_5_av.m3u8?null=
#EXT-X-STREAM-INF:SUBTITLES="subs",PROGRAM-ID=1,BANDWIDTH=2793000,RESOLUTION=1280x720,CODECS="avc1.64001f, mp4a.40.2"
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/index_6_av.m3u8?null=`

	r := strings.NewReader(s)
	pl, err := ParseMasterPlaylist(r)
	if err != nil {
		t.Fatalf("Unable to parse m3u %q", err)
	}

	if len(pl.Groups) != 7 {
		t.Fatalf("Didn't parse exactly 7 URIs: %d", len(pl.Groups))
	}

}
