package m3u8

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

type lineType uint

// linetypes
const (
	Tag lineType = iota
	URI
	Comment
)

// Line in M3U,
type Line struct {
	Type lineType
	Data interface{}
}

// TagArg contains the parameter values to a Tag, if any.
type TagArg string

// TagLine An EXT Tag and its arguments
type TagLine struct {
	Tag  string
	Args TagArg
}

// DecoderError contains an error message passed from LineDecoder
type DecoderError string

func (f DecoderError) Error() string {
	return string(f) //fmt.Sprintf("math: square root of negative number %g", float64(f))
}

// LineDecoder decodes data into sensible M3U lines
type LineDecoder struct {
	lines []Line
}

// NewLineDecoder create a new LineDecoder
func NewLineDecoder() *LineDecoder {
	return &LineDecoder{}
}

// Write data to decoder and parse into m3u lines.
func (ld *LineDecoder) Write(p []byte) (int, error) {

	if len(p) == 0 {
		return 0, nil
	}

	var buffer bytes.Buffer
	scanner := bufio.NewScanner(&buffer)

	// Store data in our internal buffer.
	n, err := buffer.Write(p)
	if err != nil {
		log.Printf("buffer write err %q", err)
		return n, DecoderError(fmt.Sprintf("Buffer write err: %q", err))
	}

	for scanner.Scan() {
		l := strings.TrimSpace(scanner.Text())

		if len(l) == 0 { // Trimmed line of nothing
			continue
		}

		var line Line

		// EXT tag
		if strings.HasPrefix(l, "#EXT") {

			ext := decodeExtTag(l)
			line = Line{Type: Tag, Data: ext}

			// It's a comment
		} else if strings.HasPrefix(l, "#") {
			l = strings.TrimSpace(strings.TrimPrefix(l, "#"))
			line = Line{Type: Comment, Data: l}

			// Must be a URI
		} else {
			line = Line{Type: URI, Data: l}
		}

		ld.lines = append(ld.lines, line)
	}
	if err := scanner.Err(); err != nil {
		log.Printf("Scanner error: %q", err)
	}

	return n, nil
}

// GetLines Returns all decoded lines
func (ld *LineDecoder) GetLines() []Line {
	return ld.lines
}

var reDecExt = regexp.MustCompile("^#([^:|$]+):*([^$]*)")

func decodeExtTag(l string) TagLine {

	// Find #<tag>:<args>
	t := reDecExt.FindAllStringSubmatch(l, 2)
	tag := t[0][1]
	args := t[0][2]

	return TagLine{Tag: tag, Args: TagArg(args)}

}

var reAttrs = regexp.MustCompile("([A-Z_-]+)=(\"[^\"]+\"|[^\",]+)")

// AttrList Parses TagArg and returns a map of attributes if there are any.
func (ta *TagArg) AttrList() (map[string]string, error) {

	// Try to parse args as an attribute list.
	keyVals := reAttrs.FindAllStringSubmatch(string(*ta), -1)
	if len(keyVals) == 0 {
		// Not an attribute list
		return nil, fmt.Errorf("Argument is not an attribute list")
	}

	// Create map out of it
	attrs := make(map[string]string)
	for _, v := range keyVals {
		attrs[v[1]] = strings.Trim(v[2], "\"")
	}

	return attrs, nil
}

// Read all of io.Reader and Line decode the data.
func decode(r io.Reader) (*LineDecoder, error) {
	ld := NewLineDecoder()

	// Read all of reader into buffer.
	// LineDecoder scanner wants a complete set of data
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	br := bytes.NewReader(b)

	w, err := io.Copy(ld, br)
	if err != nil {
		return nil, fmt.Errorf("Error writing all to decoder: %q", err)
	}

	if int(w) != len(b) {
		return nil, fmt.Errorf("Didn't copy all data %d/%d", w, len(b))
	}

	return ld, nil
}
