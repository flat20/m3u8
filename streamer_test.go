package m3u8

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"testing"
	"time"
)

func ExampleMediaReader() {

	// Taken from Apple's example .m3u8
	var s = `#EXTM3U
#EXT-X-TARGETDURATION:10
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
#EXTINF:9.97667,
https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear1/fileSequence0.ts
#EXTINF:9.97667,
https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear1/fileSequence1.ts
#EXTINF:9.97667,
https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear1/fileSequence2.ts
#EXTINF:9.97667,
https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear1/fileSequence3.ts`

	r := strings.NewReader(s)

	pl, err := ParseMediaPlaylist(r)
	client := &http.Client{Timeout: time.Second * 20}
	ps := NewMediaReader(pl, client, pl.Sequence)

	_, err = io.Copy(ioutil.Discard, ps)
	if err != nil {
		fmt.Printf("Error copying: %q", err)
	}

	// Output:
}

func TestParseMediaPlaylist(t *testing.T) {

	var s = `#EXTM3U
#EXT-X-TARGETDURATION:10
#EXT-X-ALLOW-CACHE:YES
#EXT-X-PLAYLIST-TYPE:VOD
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:1
#EXTINF:10.000,
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/segment1_6_av.ts?null=
#EXTINF:10.000,
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/segment2_6_av.ts?null=
#EXTINF:10.000,
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/segment3_6_av.ts?null=
#EXTINF:10.000,
http://svtplay12s-f.akamaihd.net/i/world/open/20150528/1207696-001A/EPISOD-1207696-001A-654a8486aaee5d41_,892,144,252,360,540,1584,2700,.mp4.csmil/segment4_6_av.ts?null=`

	r := strings.NewReader(s)
	pl, err := ParseMediaPlaylist(r)
	if err != nil {
		t.Fatalf("Unable to parse media playlist %q", err)
	}

	if len(pl.Segments) != 4 {
		t.Fatalf("Incorrect number of segments")
	}

	if pl.Sequence != 1 {
		t.Fatalf("Sequence isn't 1")
	}
}

func TestMediaReader(t *testing.T) {
	client := &http.Client{Timeout: time.Second * 5}

	// 'Advanced' version which contains byte range EXT tags, which we don't
	// support currently.
	// #EXT-X-BYTERANGE: length[@offset]

	// #EXTINF:9.9766,
	// #EXT-X-BYTERANGE:326744@0
	// main.ts
	// #EXTINF:9.9766,
	// #EXT-X-BYTERANGE:326368@326744
	// main.ts
	// #EXTINF:10.01,
	// #EXT-X-BYTERANGE:327120@653112
	// main.ts
	//url := "https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8"

	// This is the standard version
	url := "https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8"
	br, err := loadURL(client, url)
	if err != nil {
		t.Fatalf("Unable to load master m3u8 from Apple. %q", err)
	}
	defer br.Close()

	m, err := ParseMasterPlaylist(br)
	if err != nil {
		t.Fatalf("Unable to parse master playlist. %q", err)
	}

	// Load media list from first option in master playlist.
	murl, _ := makeAbs(url, m.Groups[0].URI)
	mr, err := loadURL(client, murl)
	if err != nil {
		t.Fatalf("Unable to load media m3u8. %q", err)
	}
	defer mr.Close()

	// Parse media list and fix relative URIs.
	pl, err := ParseMediaPlaylist(mr)
	if err != nil {
		t.Fatalf("Unable to parse media playlist. %q", err)
	}

	for i, v := range pl.Segments {
		pl.Segments[i].URI, _ = makeAbs(murl, v.URI)
	}

	s := NewMediaReader(pl, client, 0)
	w, err := copyProg(ioutil.Discard, s)
	if err != nil {
		t.Fatalf("Unable to stream. %q", err)
	}

	log.Printf("Wrote: %d", w)

}

// io.Copy but print progress
func copyProg(dst io.Writer, src *MediaReader) (written int64, err error) {
	buf := make([]byte, 32*1024)
	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
				fmt.Printf("\rWrote %.2f MB Sequence: %d / %d", float64(written)/1024/1024, src.sequence, src.end)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er == io.EOF {
			break
		}
		if er != nil {
			err = er
			break
		}
	}
	return written, err
}
