package m3u8

import (
	"bytes"
	"io"
	//"log"
	"testing"
)

var lines = []byte(`one
two
three`)

var single = []byte(`myfile.avi`)

func TestLines(t *testing.T) {

	ld := NewLineDecoder()
	ldcopy(ld, lines, t)
	if len(ld.GetLines()) != 3 {
		t.Fatalf("GetLines %d not 3", len(ld.GetLines()))
	}

}

// Splitting lines in half will count as two lines
func TestHalfWrite(t *testing.T) {

	ld := NewLineDecoder()
	ldcopy(ld, lines[0:6], t)
	ldcopy(ld, lines[6:], t)

	if len(ld.GetLines()) != 4 {
		t.Fatalf("GetLines %d not 3", len(ld.GetLines()))
	}

}

func ldcopy(ld *LineDecoder, in []byte, t *testing.T) {

	r := bytes.NewReader(in)

	w, err := io.Copy(ld, r)
	if err != nil {
		t.Fatalf("Copy %q", err)
	}

	if int(w) != len(in) {
		t.Fatalf("Copy only wrote %d/%d bytes", w, len(in))
	}

}
